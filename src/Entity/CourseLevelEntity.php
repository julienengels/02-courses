<?php

namespace App\Entity;

use App\Repository\CourseLevelEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseLevelEntityRepository::class)
 */
class CourseLevelEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prerequisite;

    /**
     * @ORM\OneToMany(targetEntity=CourseEntity::class, mappedBy="level")
     */
    private $courseEntities;

    public function __construct()
    {
        $this->courseEntities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrerequisite(): ?string
    {
        return $this->prerequisite;
    }

    public function setPrerequisite(string $prerequisite): self
    {
        $this->prerequisite = $prerequisite;

        return $this;
    }

    /**
     * @return Collection|CourseEntity[]
     */
    public function getCourseEntities(): Collection
    {
        return $this->courseEntities;
    }

    public function addCourseEntity(CourseEntity $courseEntity): self
    {
        if (!$this->courseEntities->contains($courseEntity)) {
            $this->courseEntities[] = $courseEntity;
            $courseEntity->setLevel($this);
        }

        return $this;
    }

    public function removeCourseEntity(CourseEntity $courseEntity): self
    {
        if ($this->courseEntities->contains($courseEntity)) {
            $this->courseEntities->removeElement($courseEntity);
            // set the owning side to null (unless already changed)
            if ($courseEntity->getLevel() === $this) {
                $courseEntity->setLevel(null);
            }
        }

        return $this;
    }
}
