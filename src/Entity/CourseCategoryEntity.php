<?php

namespace App\Entity;

use App\Repository\CourseCategoryEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseCategoryEntityRepository::class)
 */
class CourseCategoryEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=CourseEntity::class, mappedBy="category")
     */
    private $courseEntities;

    public function __construct()
    {
        $this->courseEntities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|CourseEntity[]
     */
    public function getCourseEntities(): Collection
    {
        return $this->courseEntities;
    }

    public function addCourseEntity(CourseEntity $courseEntity): self
    {
        if (!$this->courseEntities->contains($courseEntity)) {
            $this->courseEntities[] = $courseEntity;
            $courseEntity->setCategory($this);
        }

        return $this;
    }

    public function removeCourseEntity(CourseEntity $courseEntity): self
    {
        if ($this->courseEntities->contains($courseEntity)) {
            $this->courseEntities->removeElement($courseEntity);
            // set the owning side to null (unless already changed)
            if ($courseEntity->getCategory() === $this) {
                $courseEntity->setCategory(null);
            }
        }

        return $this;
    }
}
