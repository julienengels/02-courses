<?php

namespace App\Repository;

use App\Entity\CourseEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseEntity[]    findAll()
 * @method CourseEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseEntity::class);
    }

    // /**
    //  * @return CourseEntity[] Returns an array of CourseEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseEntity
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
